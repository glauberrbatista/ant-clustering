from formigas import formiga_viva, formiga_morta
from termcolor import colored
import math
import random

class formigueiro(object):
	matriz = [[]]
	linhas = 0
	colunas = 0
	formigas_vivas = []
	formigas_mortas = []
	n_formigas_vivas = 0
	raio = 0
	iteracoes = 0

	def __init__(self, linhas, colunas, raio, iteracoes, arq_form_mortas, form_vivas):
		super(formigueiro, self).__init__()
		self.linhas = linhas
		self.colunas = colunas
		#cria uma formiga morta vazia e usa ela para popular a matriz
		f = formiga_morta(0,[])
		self.matriz = [[f for i in range(linhas)] for j in range(colunas)]
		self.raio = raio
		self.iteracoes = iteracoes
		self.n_formigas_vivas = form_vivas

		usados = []
		arquivo = open(arq_form_mortas, "r")
		lista_itens = []
		for linha in arquivo:
			valido = False
			valores = linha.split()
			tipo = int(valores[-1])
			dados = [float(i) for i in valores[:-1]]
			lista_itens.append(dados)
			while not valido:
				f = formiga_morta(tipo, dados)
				coord = [random.randint(0, linhas-1),random.randint(0, colunas-1)]
				if coord not in usados:
					self.matriz[coord[0]][coord[1]] = f
					usados.append(coord)
					valido = True

		#calcula maior distancia para a normalizacao
		soma = 0
		for i in range(len(lista_itens[0])):
			maior_aux = -9999
			menor_aux = 9999
			for j in lista_itens:
				if j[i] > maior_aux:
					maior_aux = j[i]
				if j[i] < menor_aux:
					menor_aux = j[i]
			soma += (maior_aux - menor_aux)**2

		maior_distancia = math.sqrt(soma)

		
		# posiciona as formigas vivas aleatoriamente, de modo que nao possa haver duas formigas no mesmo local
		usados = []
		for i in range(0, self.n_formigas_vivas):
			valido = False
			while not valido:
				f = formiga_viva(random.randint(0, linhas-1), random.randint(0, colunas-1), raio, linhas, colunas, maior_distancia)
				if [f.x, f.y] not in usados:
					self.formigas_vivas.append(f)
					usados.append([f.x,f.y])
					valido = True

	# as funcoes abaixo dessa linha servem apenas para mostrar o comportamento no terminal, sem interface grafica
	def inicia_processo(self):
		self.print_matriz()
		for i in range(self.iteracoes):
			escolhida = random.randint(0, len(self.formigas_vivas)-1)
			self.formigas_vivas[escolhida].pegar(self.matriz)
		self.print_matriz()
			

	def print_matriz(self):
		for i in range(self.linhas):
			linha = ""
			for j in range(self.colunas):
				if self.matriz[i][j] == 0:
					t = ' '
				else:
					t = colored('*', 'white')
				linha += t
			print linha
		print "-------------------------"