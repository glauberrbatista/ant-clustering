import random
from PyQt4.QtGui import *
import logging
import math
#logging.basicConfig(filename='formigas.log',level=logging.DEBUG)

cores = {
	0 : QColor(255,255,255), #branco
	1 : QColor(255,0,0), #vermelho
	2 : QColor(0,255,0), #verde
	3 : QColor(0,0,255), #azul
	4 : QColor(255,255,0), #amarelo
	5 : QColor(255,0,255), #magenta
	6 : QColor(0,255,255), #ciano
	7 : QColor(255,128,0), #laranja
	8 : QColor(128,255,0), #verde limao
	9 : QColor(0,128,255), #azul claro
	10 : QColor(127,0,255), #roxo
	11 : QColor(255,0,127), #purpura
	12 : QColor(102,51,0), #marrom
	13 : QColor(0,25,51), #azul marinho
	14 : QColor(105,105,105), #cinza
	15 : QColor(250,128,114), #salmao
	16 : QColor(0,100,0) #verde escuro
}

class formiga_morta(object):
	
	tipo = 0
	dados = []
	def __init__(self, tipo, dados):
		super(formiga_morta, self).__init__()
		self.tipo = tipo
		self.dados = dados

class formiga_viva(object):

	x = 0
	y = 0
	r = 0
	carregando = False
	item = None
	alfa = 0.0
	n_fail = 0
	movimentos = 0
	max_lin = 0
	max_col = 0

	maior_distancia = 0
	
	def __init__(self, _x, _y, _r, lin, col, m_distancia):
		super(formiga_viva, self).__init__()
		self.x = _x
		self.y = _y
		self.r = _r
		self.max_lin = lin
		self.max_col = col
		self.alfa = random.uniform(0,1)
		self.maior_distancia = m_distancia

	def proporcao_formigas_raio(self, formigueiro):
		n_formigas = 0

		i_min = self.x - self.r
		i_max = self.x + self.r + 1
		j_min = self.y - self.r
		j_max = self.y + self.r + 1

		for i in range(i_min, i_max):
			for j in range(j_min, j_max):
				# usando o mod "%" eh possivel contar as formigas do outro lado da matriz, simulando uma esfera
				if formigueiro[i % self.max_lin][j % self.max_col] == 1:
					n_formigas += 1

		n_formigas -= 1
		# formula contar quadrados: ((2 * float(self.r) + 1) ** 2 - 1)
		prop = float(n_formigas) / ((2 * float(self.r) + 1) ** 2 - 1)

		# se a proporcao for menor que 1% ou maior que 99%, eh somado ou diminuido 1%, para nao ser determinista a tomada de decisao
		if prop < 0.01:
			prop += 0.01
		if prop > 0.99:
			prop -= 0.01
		
		return float('%.2f' % prop)

	def distancia(self, formiga1, formiga2):
		soma = 0.0
		#logging.debug('--f1 %d, f2 %d' % (formiga1.tipo, formiga2.tipo))
		for i in range(len(formiga1.dados)):
			xi = float(formiga1.dados[i] - formiga2.dados[i])**2
			soma += xi
		#logging.debug('-- Soma (dist): %f' % soma)
		dist = math.sqrt(soma) / self.maior_distancia
		#logging.debug('--Dist %f' % dist)
		return dist

	def similaridade(self, formiga, formigueiro):
		i_min = self.x - self.r
		i_max = self.x + self.r + 1
		j_min = self.y - self.r
		j_max = self.y + self.r + 1

		soma_distancias = 0

		for i in range(i_min, i_max):
			for j in range(j_min, j_max):
				if i == self.x and j == self.y:
					continue
				vizinha = formigueiro[i % self.max_lin][j % self.max_col]
				if vizinha.tipo != 0:
					dist = 1 - self.distancia(formiga, vizinha) / 0.1
					#logging.debug('-- 1-dist/alfa: %f' % dist)
					soma_distancias += dist

		#logging.debug('-- soma 1-dist/alfa: %f' %soma_distancias)
		coeficiente = (1 / float((2 * float(self.r) + 1) ** 2)) * float(soma_distancias)
		#logging.debug('--Coef sim: %f' % coeficiente)

		if coeficiente > 0:
			return coeficiente
		else:
			return 0

	def probabilidade_pegar(self, formigueiro):
		formiga = formigueiro[self.x][self.y]
		k1 = 0.1
		sim = self.similaridade(formiga, formigueiro)
		'''if sim <= 1.0:
			probabilidade = 1.0
		else:
			#probabilidade = 1/(sim**2)
			probabilidade = (k1 / (k1 + sim))**2'''
		probabilidade = (k1 / (k1 + sim))**2
		#logging.debug('--Sim: %f' % sim)
		#logging.debug('--ProbP: %f' % probabilidade)
		return probabilidade

	def probabilidade_soltar(self, formigueiro):
		formiga = self.item
		k2 = 0.5
		sim = self.similaridade(formiga, formigueiro);

		if sim >= 1.0:
			probabilidade = 1.0
		else:
			#probabilidade = sim**4
			probabilidade = (sim / (k2 + sim))**2
		#logging.debug('--ProbS: %f' % probabilidade)
		return probabilidade

	def movimento(self, formigueiro, qt_tabela):
		#adapta o alfa depois de 100 movimentos. sec. 4.6.2 do artigo
		'''if self.movimentos >= 100:
			r_fail = self.n_fail/100
			if r_fail>0.99:
				self.alfa += 0.01
			else:
				self.alfa -= 0.01'''
#			logging.debug('--novo alfa: %f' % self.alfa)

		self.movimentos += 1
		while True:
			if not self.carregando:
				self.pegar(formigueiro, qt_tabela)
			else:
				if self.soltar(formigueiro, qt_tabela):
					return
			self.andar(formigueiro)


	def pegar(self, formigueiro, qt_tabela):
#		logging.debug('--Pegar')
		item_pos = formigueiro[self.x][self.y]
		if item_pos.tipo != 0 and not self.carregando:
			chance = float('%.2f' % random.uniform(0,1))
			probabilidade = self.probabilidade_pegar(formigueiro)
			#logging.debug('--Chance de pegar: %f' % chance)
			#logging.debug('--Prob de pegar: %f' % probabilidade)
			if 0.00 <= chance <= probabilidade:

				# atualiza a tabela do Qt, pintando o quadrado atual de branco
				qt_tabela.item(self.x,self.y).setBackground(cores[0])

				self.item = formigueiro[self.x][self.y]
				formigueiro[self.x][self.y] = formiga_morta(0, [])
				self.carregando = True
#				logging.debug('--Peguei')


	def andar(self, formigueiro):
		options = {
			0 : [(self.x - 1) % self.max_lin , (self.y - 1)     % self.max_col] ,
			1 : [(self.x - 1) % self.max_lin , (self.y)         % self.max_col] ,
			2 : [(self.x - 1) % self.max_lin , (self.y + 1)     % self.max_col] ,
			3 : [(self.x)     % self.max_lin , (self.y - 1)     % self.max_col] ,
			4 : [(self.x)     % self.max_lin , (self.y + 1)     % self.max_col] ,
			5 : [(self.x + 1) % self.max_lin , (self.y - 1)     % self.max_col] ,
			6 : [(self.x + 1) % self.max_lin , (self.y)         % self.max_col] ,
			7 : [(self.x + 1) % self.max_lin , (self.y + 1)     % self.max_col]
		}

		direcao = random.randint(0,7)
		temp_x, temp_y = options[direcao]
		self.x = temp_x
		self.y = temp_y
#		logging.debug('--Andei para: [%d, %d]' % (self.x, self.y))

	def soltar(self, formigueiro, qt_tabela):
		#logging.debug('--Soltar')
		item_pos = formigueiro[self.x][self.y]
		if self.carregando and item_pos.tipo == 0:
			chance = float('%.2f' % random.uniform(0,1))
			probabilidade = self.probabilidade_soltar(formigueiro)
#			logging.debug('--Chance de soltar: %f' % chance)
#			logging.debug('--Prob de soltar: %f' % probabilidade)
			if 0.00 <= chance <= probabilidade:

				# atualiza a tabela do Qt, pintando o quadrado atual de vermelho
				qt_tabela.item(self.x,self.y).setBackground(cores[self.item.tipo])
				formigueiro[self.x][self.y] = self.item
				self.item = None
				self.carregando = False
#				logging.debug('--Soltei')
				return True
		return False


