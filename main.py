from interface_qt import InterfaceQt
import random
import sys
import json
import argparse

sys.setrecursionlimit(10000)

def main():
	arquivo_config = args.input[0]
	arquivo_dados = args.dados[0]

	with open(arquivo_config) as data_file:
		entrada = json.load(data_file)

	#interface = Interface(entrada['x'], entrada['y'], entrada['raio'], entrada['it'], entrada['formigas_mortas'], entrada['formigas_vivas'])
	#interface.executa()

	interface = InterfaceQt(entrada['x'], entrada['y'], entrada['raio'], entrada['it'], arquivo_dados, entrada['formigas_vivas'])
	interface.executa()

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Ant Clustering')
	parser.add_argument('-i', dest='input', action='store', nargs=1, help='Arquivo de entrada (configuracoes)')
	parser.add_argument('-d', dest='dados', action='store', nargs=1, help='Arquivo de dados')

	args = parser.parse_args()

	main()

