from PyQt4.QtGui import * 
from PyQt4.QtCore import *
from formigueiro import formigueiro
import time
import random
import sys

cores = {
	0 : QColor(255,255,255), #branco
	1 : QColor(255,0,0), #vermelho
	2 : QColor(0,255,0), #verde
	3 : QColor(0,0,255), #azul
	4 : QColor(255,255,0), #amarelo
	5 : QColor(255,0,255), #magenta
	6 : QColor(0,255,255), #ciano
	7 : QColor(255,128,0), #laranja
	8 : QColor(128,255,0), #verde limao
	9 : QColor(0,128,255), #azul claro
	10 : QColor(127,0,255), #roxo
	11 : QColor(255,0,127), #purpura
	12 : QColor(102,51,0), #marrom
	13 : QColor(0,25,51), #azul marinho
	14 : QColor(105,105,105), #cinza
	15 : QColor(250,128,114), #salmao
	16 : QColor(0,100,0) #verde escuro
}

class InterfaceQt(object):
	
	linhas = 0
	colunas = 0
	raio = 0
	iteracoes = 0
	arq_mortas = None
	n_vivas = 0
	tabela = None
	app = None

	def __init__(self, x, y, r, it, arq_mortas, vivas):
		super(InterfaceQt, self).__init__()
		self.linhas = x
		self.colunas = y
		self.raio = r
		self.iteracoes = it
		self.arq_mortas = arq_mortas
		self.n_vivas = vivas
		
		# Criacao da aplicacao e da tabela
		self.app 	= QApplication(sys.argv)
		self.tabela 	= QTableWidget()

		self.tabela.setWindowTitle("Ant Colony Optimization")
		self.tabela.setRowCount(self.linhas)
		self.tabela.setColumnCount(self.colunas)

		header = self.tabela.horizontalHeader()
		vheader = self.tabela.verticalHeader()

		# configurar para fixo o modo das colunas e linhas
		header.setResizeMode(QHeaderView.Fixed)
		vheader.setResizeMode(QHeaderView.Fixed)
		vheader.setVisible(False)
		header.setVisible(False)

		# redimensionar cada coluna e linha para tamanho 7
		for i in range(self.linhas):
			header.resizeSection(i,7)
			vheader.resizeSection(i,7)

		# calcular e definir a largura da tabela (e consequentemente, da janela)
		width = header.length() + 5
		if self.tabela.verticalScrollBar().isVisible():
			width += self.tabela.verticalScrollBar().width()
		width += self.tabela.frameWidth() * 2
		self.tabela.setFixedWidth(width)

		# calcular e definir a altura da tabela (e consequentemente, da janela)
		height = vheader.length() + 5
		if self.tabela.horizontalScrollBar().isVisible():
			height += self.tabela.horizontalScrollBar.height()
		self.tabela.setFixedHeight(height)

		#inicializar as celulas
		for i in range(self.linhas):
			for j in range(self.colunas):
				self.tabela.setItem(i,j, QTableWidgetItem(""))
				#self.tabela.setCellWidget(i, j, ImgWidget(0, self.tabela))

		self.tabela.setEditTriggers(QAbstractItemView.NoEditTriggers)
		self.tabela.show()

	def update(self, formiga):
		img_index = 0
		color = WHITE
		soltou = False
		while True:
			if not formiga.carregando:
				if formiga.pegar(self.form.matriz):
					img_index = 2
				else:
					img_index = 1
				
			else:
				if formiga.soltar(self.form.matriz):
					img_index = 1
					soltou = True
				else:
					img_index = 2

			if self.form.matriz[formiga.x][formiga.y] == 1:
				color = RED
			else:
				color = WHITE

			self.tabela.item(formiga.x, formiga.y).setBackground(color)
			if soltou:
				self.tabela.cellWidget(formiga.x, formiga.y).update(img_index, self.tabela)
				self.tabela.update()
				return True
			
			coord = formiga.andar(self.form.matriz)
			self.tabela.cellWidget(coord[0], coord[1]).update(0, self.tabela)
			self.tabela.cellWidget(formiga.x, formiga.y).update(img_index, self.tabela)
			self.tabela.update()


	def executa(self):
		self.form = formigueiro(self.linhas, self.colunas, self.raio, self.iteracoes, self.arq_mortas, self.n_vivas)

		for row in range(self.linhas):
			for column in range(self.colunas):
				formiga = self.form.matriz[row][column]
				self.tabela.item(row,column).setBackground(cores[formiga.tipo])

		self.tabela.show()
		#self.app.exec_()
		# estado inicial do formigueiro
		for i in range(self.iteracoes):
			escolhida = random.randint(0, len(self.form.formigas_vivas)-1)
			self.form.formigas_vivas[escolhida].movimento(self.form.matriz, self.tabela)
			self.app.processEvents()

		# invocado aqui so para mostrar o resultado final, caso a janela seja fechada
		self.tabela.show()
		return self.app.exec_()