# Ant Clustering

Este trabalho está sendo realizado para disciplina de Inteligência Artificial do Curso de Ciência da Computação da UDESC.

O trabalho encontra-se em constante desenvolvimento. Isso quer dizer que alguns bugs podem existir e devem ser reparados ao longo do tempo.

Para executar o código é necessário ter o PyQt 4 instalado. Com algumas alterações no código é possível utilizar o PyGame, que já possui uma classe implementada, mas é muito lento para o propósito.

Os parâmetros são configurados através do arquivo "entrada.json".

Para executar o código, basta executar a partir do terminal: python main.py -i entrada.json -d dados.txt.

O parâmetro -i indica o arquivo com as configurações de raio, número de formigas vivas, raio e movimentos.
O parâmetro -d indica os dados utilizados para classificação. Obrigatoriamente, o último valor da linha deve ser um índice de grupo, utilizado para pintar os itens do mesmo grupo na grid.

Existem 17 cores predefinidas: a cor no índice 0 é a cor branca, utilizada para o background do grid. As demais cores são utilizadas para a representação dos dados.