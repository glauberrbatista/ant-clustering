from formigueiro import formigueiro
import random
import pygame
import time
from pygame.locals import *
import sys
sys.setrecursionlimit(10000)

class Interface(object):
	#colors
	PRETO = (0,0,0)
	BRANCO = (255,255,255)
	VERMELHO = (255,0,0)
	VERDE = (0,255,0)
	AZUL = (0,0,255)

	CELULA = 4

	linhas = 0
	colunas = 0
	raio = 0
	iteracoes = 0
	form = None
	janela = None
	clock = None
	n_mortas = 0
	n_vivas = 0

	def __init__(self, x, y, r, it, mortas, vivas):
		super(Interface, self).__init__()
		self.linhas = x
		self.colunas = y
		self.raio = r
		self.iteracoes = it
		self.n_mortas = mortas
		self.n_vivas = vivas
		pygame.init()

		TAMANHO_JANELA = [self.linhas*self.CELULA, self.colunas*self.CELULA]
		self.janela = pygame.display.set_mode(TAMANHO_JANELA, DOUBLEBUF|HWSURFACE)
		self.janela.set_alpha(None)
		pygame.display.set_caption("Ant Colony Optimization")
		self.clock = pygame.time.Clock()
		pygame.event.set_allowed([QUIT])


	def executa(self):
		self.form = formigueiro(self.linhas, self.colunas, self.raio, self.iteracoes, self.n_mortas, self.n_vivas)

		fechar = False
		self.janela.fill(self.PRETO)
		for row in range(self.linhas):
			for column in range(self.colunas):
				color = self.BRANCO
				if self.form.matriz[row][column] == 1:
					color = self.VERMELHO
				pygame.draw.rect(self.janela, color, [self.CELULA * column, self.CELULA * row, self.CELULA, self.CELULA])
		pygame.display.flip()

		for i in range(self.iteracoes):
			escolhida = random.randint(0, len(self.form.formigas_vivas)-1)
			self.form.formigas_vivas[escolhida].pegar(self.form.matriz, self.janela)
			

		time.sleep(60)
		pygame.quit()